# Sindbad's Travel Adventures

## Overview

Sindbad's Travel Adventures is a Streamlit-based web application that helps users plan their travel adventures. It provides personalized travel plans based on user preferences, including flight and hotel recommendations, weather forecasts, travel alerts, and restaurant suggestions.

## Features

- Personalized travel plan generation
- Weather forecast for the destination
- Travel safety alerts
- Flight offers
- Hotel recommendations
- Restaurant suggestions
- Interactive user interface using Streamlit

## Project Structure

```
sindbad_app/
│
├── sindbad_app_beta.py    # Main application file for Amazon Bedrock
├── .env                   # Environment variables (not in version control)
├── requirements.txt       # Project dependencies
└── README.md              # Project documentation
```

## Setup

1. Clone the repository:
   ```
   git clone https://gitlab.com/goldspell/sindbad.git
   cd sindbad
   ```

2. Create a virtual environment and activate it:
   ```
   python -m venv venv
   source venv/bin/activate  # On Windows, use `venv\Scripts\activate`
   ```

3. Install the required dependencies:
   ```
   pip install -r requirements.txt
   ```

4. Create a `.env` file in the project root and add your API keys:
   ```
   AWS_ACCESS_KEY_ID=your_aws_access_key
   AWS_SECRET_ACCESS_KEY=your_aws_secret_key
   ANTHROPIC_API_KEY=your_anthropic_api_key
   TOMORROW_API_KEY=your_tomorrow_api_key
   AMADEUS_API_KEY=your_amadeus_api_key
   AMADEUS_API_SECRET=your_amadeus_api_secret
   YELP_API_KEY=your_yelp_api_key
   ```

## Usage

To run the application, use the following command:

```
streamlit run sindbad_app_beta.py # for Amazon bedrock Claude 3 or  streamlit run sindbad_app_beta_v02.py # for Anthropic Claude 3.5 Sonnet API
```

The application will open in your default web browser. Follow these steps to use the app:

1. Enter your name, origin (airport code), and destination (city name).
2. Select your departure and return dates.
3. Set your budget using the slider.
4. Choose your interests from the provided options.
5. Click on "Generate Travel Plan" to create your personalized adventure.

## Components

### Main Application (`sindbad_app_beta.py`)

This file contains the entire application logic, including:

- API integrations (Amazon Bedrock, Anthropic, Tomorrow.io, Amadeus, Yelp)
- Data fetching functions for weather, travel alerts, flights, hotels, and restaurants
- Streamlit UI components
- Travel plan generation using AWS Bedrock's Anthropic Claude 3 model or Anthropic Claude 3.5 Sonnet model

### Environment Variables (`.env`)

Store your API keys and secrets in this file. Do not commit this file to version control.

### Requirements (`requirements.txt`)

Lists all the Python packages required to run the application.

## API Integrations

- **Amazon Bedrock Anthropic Claude 3**: Used for generating personalized travel plans.
- **Anthropic Claude 3.5 Sonnet**: Used for generating personalized travel plans.
- **Tomorrow.io**: Provides weather forecast data.
- **Amadeus**: Used for flight and hotel offers.
- **Yelp**: Provides restaurant recommendations.
- **Travel Advisory**: Offers travel safety information.

## Troubleshooting

- If you encounter API-related errors, ensure that your API keys in the `.env` file are correct and have the necessary permissions.
- For issues with location data, try using more specific or well-known city names.
- If the application fails to generate a travel plan, check your internet connection and try again.


