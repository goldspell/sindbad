import streamlit as st
import os
import json
from datetime import datetime, timedelta
from dotenv import load_dotenv
from geopy.geocoders import Nominatim
import certifi
import ssl
import requests
from anthropic import Anthropic, HUMAN_PROMPT, AI_PROMPT

# Load environment variables
load_dotenv()

# Anthropic client setup
anthropic = Anthropic(api_key=os.getenv("ANTHROPIC_API_KEY"))

# API keys
TOMORROW_API_KEY = os.getenv("TOMORROW_API_KEY")
AMADEUS_API_KEY = os.getenv("AMADEUS_API_KEY")
AMADEUS_API_SECRET = os.getenv("AMADEUS_API_SECRET")
YELP_API_KEY = os.getenv("YELP_API_KEY")

ssl_context = ssl.create_default_context(cafile=certifi.where())

@st.cache_data(ttl=3600)
def generate_travel_plan(user_input, additional_info):
    try:
        prompt = f"{HUMAN_PROMPT}As Sindbad the sailor, create an engaging and detailed travel plan based on these preferences:\n\n{user_input}\nAdditional information:\n{additional_info}\n\nInclude recommendations for flights, accommodations, transportation, weather considerations, local cuisine, historical sites, and safety information. Incorporate the provided flight and hotel prices, restaurant recommendations, and travel alerts into your story. Format your response as a captivating adventure story.{AI_PROMPT}"

        response = anthropic.completions.create(
            model="claude-3-5-sonnet-20240620",
            prompt=prompt,
            max_tokens_to_sample=2000,
            temperature=0.7,
        )

        return response.completion

    except Exception as e:
        st.error(f"An error occurred while generating the travel plan: {str(e)}")
        return None

@st.cache_data(ttl=3600)
def get_weather_forecast(latitude, longitude):
    url = "https://api.tomorrow.io/v4/timelines"
    querystring = {
        "location": f"{latitude},{longitude}",
        "fields": ["temperature", "humidity", "windSpeed", "precipitationProbability"],
        "units": "metric",
        "timesteps": "1d",
        "apikey": TOMORROW_API_KEY
    }

    try:
        response = requests.get(url, params=querystring)
        response.raise_for_status()
        data = response.json()
        daily_data = data['data']['timelines'][0]['intervals']

        forecast = []
        for day in daily_data[:7]:  # 7-day forecast
            date = datetime.fromisoformat(day['startTime']).strftime('%Y-%m-%d')
            forecast.append({
                'date': date,
                'temperature': round(day['values']['temperature']),
                'humidity': round(day['values']['humidity']),
                'wind_speed': round(day['values']['windSpeed']),
                'precipitation_probability': round(day['values']['precipitationProbability'])
            })

        return forecast
    except requests.exceptions.RequestException as e:
        st.error(f"Error fetching weather data: {str(e)}")
        return None

@st.cache_data(ttl=3600)
def get_travel_alerts(country):
    url = "https://www.travel-advisory.info/api"
    
    try:
        response = requests.get(url)
        response.raise_for_status()
        data = response.json()
        country_data = data['data']
        
        for country_code, country_info in country_data.items():
            if 'name' in country_info and country_info['name'].lower() == country.lower():
                return country_info['advisory']
        return None
    except requests.exceptions.RequestException as e:
        st.error(f"Error fetching travel alerts: {str(e)}")
        return None

@st.cache_data(ttl=3600)
def get_amadeus_access_token():
    url = "https://test.api.amadeus.com/v1/security/oauth2/token"
    data = {
        "grant_type": "client_credentials",
        "client_id": AMADEUS_API_KEY,
        "client_secret": AMADEUS_API_SECRET
    }
    response = requests.post(url, data=data)
    response.raise_for_status()
    return response.json()["access_token"]

@st.cache_data(ttl=3600)
def get_flight_offers(origin, destination, departure_date):
    access_token = get_amadeus_access_token()
    url = f"https://test.api.amadeus.com/v2/shopping/flight-offers"
    headers = {"Authorization": f"Bearer {access_token}"}
    params = {
        "originLocationCode": origin,
        "destinationLocationCode": destination,
        "departureDate": departure_date,
        "adults": 1,
        "max": 5
    }
    response = requests.get(url, headers=headers, params=params)
    response.raise_for_status()
    return response.json()["data"]

@st.cache_data(ttl=3600)
def get_hotel_offers(city_code, check_in_date, check_out_date):
    access_token = get_amadeus_access_token()
    url = f"https://test.api.amadeus.com/v2/shopping/hotel-offers"
    headers = {"Authorization": f"Bearer {access_token}"}
    params = {
        "cityCode": city_code,
        "checkInDate": check_in_date,
        "checkOutDate": check_out_date,
        "adults": 1,
        "radius": 15,
        "radiusUnit": "KM",
        "paymentPolicy": "NONE",
        "includeClosed": "false",
        "bestRateOnly": "true",
        "view": "FULL",
        "sort": "PRICE",
        "max": 5
    }
    try:
        response = requests.get(url, headers=headers, params=params)
        response.raise_for_status()
        return response.json()["data"]
    except requests.exceptions.HTTPError as e:
        if e.response.status_code == 404:
            st.warning(f"No hotel offers found for city code: {city_code}. The city might not be supported or there might be no available offers.")
            return None
        else:
            st.error(f"Error fetching hotel offers: {str(e)}")
            return None
    except Exception as e:
        st.error(f"An unexpected error occurred while fetching hotel offers: {str(e)}")
        return None

@st.cache_data(ttl=3600)
def get_restaurant_recommendations(latitude, longitude):
    url = "https://api.yelp.com/v3/businesses/search"
    headers = {
        "Authorization": f"Bearer {YELP_API_KEY}"
    }
    params = {
        "latitude": latitude,
        "longitude": longitude,
        "categories": "restaurants",
        "sort_by": "rating",
        "limit": 5
    }
    response = requests.get(url, headers=headers, params=params)
    response.raise_for_status()
    return response.json()["businesses"]

@st.cache_data(ttl=3600)
def get_city_and_country_codes(city_name):
    access_token = get_amadeus_access_token()
    url = f"https://test.api.amadeus.com/v1/reference-data/locations"
    headers = {"Authorization": f"Bearer {access_token}"}
    params = {
        "keyword": city_name,
        "subType": "CITY"
    }
    response = requests.get(url, headers=headers, params=params)
    response.raise_for_status()
    data = response.json()["data"]
    if data:
        return data[0]["iataCode"], data[0]["address"]["countryCode"]
    return None, None

def main():
    st.set_page_config(page_title="Sindbad's Travel Adventures", page_icon="🧭", layout="wide")
    
    st.title("🧭 Sindbad's Travel Adventures")
    st.write("Welcome to Sindbad's Travel Adventures! Let's plan your next journey.")

    col1, col2 = st.columns(2)

    with col1:
        name = st.text_input("What's your name, adventurer?")
        origin = st.text_input("Where are you traveling from? (Airport code)")
        destination = st.text_input("Where would you like to travel? (City name)")
        departure_date = st.date_input("When do you want to depart?")
        return_date = st.date_input("When do you want to return?")
        
    with col2:
        budget = st.slider("What's your budget (in USD)?", 500, 10000, 2000)
        duration = (return_date - departure_date).days
        st.write(f"Trip duration: {duration} days")
        interests = st.multiselect("Select your interests:", 
                                   ["History", "Culture", "Nature", "Food", "Adventure", "Relaxation"])

    if st.button("Generate Travel Plan"):
        with st.spinner("Crafting your adventure..."):
            additional_info = ""

            if destination:
                geocoder = Nominatim(user_agent="sindbad_app", ssl_context=ssl_context)
                location = geocoder.geocode(destination)
                if location:
                    st.success(f"Destination found: {location.address}")
                    
                    # Get city and country codes
                    city_code, country_code = get_city_and_country_codes(destination)
                    
                    # Weather forecast
                    forecast = get_weather_forecast(location.latitude, location.longitude)
                    if forecast:
                        st.subheader("7-Day Weather Forecast")
                        forecast_info = ""
                        for day in forecast:
                            st.write(f"{day['date']}: {day['temperature']}°C, {day['precipitation_probability']}% rain chance")
                            forecast_info += f"{day['date']}: {day['temperature']}°C, {day['precipitation_probability']}% rain chance\n"
                        additional_info += f"Weather Forecast:\n{forecast_info}\n"
                    else:
                        st.warning("Couldn't fetch weather data for the specified location.")
                    
                    # Travel alerts
                    country = location.raw['address'].get('country') if 'address' in location.raw else location.raw['display_name'].split(', ')[-1]
                    
                    if country:
                        travel_alerts = get_travel_alerts(country)
                        if travel_alerts:
                            st.subheader("Travel Safety Alerts")
                            st.write(f"Advisory Score: {travel_alerts['score']}")
                            st.write(f"Message: {travel_alerts['message']}")
                            st.write(f"Source: {travel_alerts['source']}")
                            additional_info += f"Travel Alert: {travel_alerts['message']}\n"
                        else:
                            st.info("No specific travel alerts found for this destination.")
                    else:
                        st.warning("Couldn't determine the country for travel alerts.")
                    
                    # Flight offers
                    if city_code and origin:
                        flight_offers = get_flight_offers(origin, city_code, departure_date.strftime('%Y-%m-%d'))
                        if flight_offers:
                            st.subheader("Flight Offers")
                            for offer in flight_offers[:3]:
                                st.write(f"Airline: {offer['itineraries'][0]['segments'][0]['carrierCode']}")
                                st.write(f"Price: {offer['price']['total']} {offer['price']['currency']}")
                                additional_info += f"Flight: {offer['itineraries'][0]['segments'][0]['carrierCode']} - {offer['price']['total']} {offer['price']['currency']}\n"
                        else:
                            st.warning("No flight offers found for the specified route and date.")
                    
                    # Hotel offers
                    if city_code:
                        hotel_offers = get_hotel_offers(city_code, departure_date.strftime('%Y-%m-%d'), return_date.strftime('%Y-%m-%d'))
                        if hotel_offers:
                            st.subheader("Hotel Offers")
                            for offer in hotel_offers[:3]:
                                st.write(f"Hotel: {offer['hotel']['name']}")
                                st.write(f"Price: {offer['offers'][0]['price']['total']} {offer['offers'][0]['price']['currency']}")
                                additional_info += f"Hotel: {offer['hotel']['name']} - {offer['offers'][0]['price']['total']} {offer['offers'][0]['price']['currency']}\n"
                        else:
                            st.warning("No hotel offers found for the specified location and dates.")
                    
                    # Restaurant recommendations
                    restaurants = get_restaurant_recommendations(location.latitude, location.longitude)
                    if restaurants:
                        st.subheader("Restaurant Recommendations")
                        for restaurant in restaurants[:3]:
                            st.write(f"{restaurant['name']} - Rating: {restaurant['rating']}")
                            additional_info += f"Restaurant: {restaurant['name']} - Rating: {restaurant['rating']}\n"
                    else:
                        st.warning("Couldn't fetch restaurant recommendations for the specified location.")
                else:
                    st.error("Couldn't find the specified destination. Please check the city name and try again.")

            user_input = f"""
            Name: {name}
            Origin: {origin}
            Destination: {destination}
            Departure Date: {departure_date}
            Return Date: {return_date}
            Budget: ${budget}
            Duration: {duration} days
            Interests: {', '.join(interests)}
            """
            
            travel_plan = generate_travel_plan(user_input, additional_info)
            
            if travel_plan:
                st.subheader("Your Personalized Sindbad Adventure")
                st.write(travel_plan)
            else:
                st.error("Failed to generate a travel plan. Please try again later.")

if __name__ == "__main__":
    main()